package np.com.spring.bookstore.dao.impl;

import java.util.List;

import np.com.spring.bookstore.dao.UserDao;
import np.com.spring.bookstore.model.User;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	public void saveUser(User user) {
		getSession().merge(user);

	}

	@SuppressWarnings("unchecked")
	public List<User> listUsers() {
		return getSession().createCriteria(User.class).list();
	}

	public User getUser(Long id) {
		return (User) getSession().get(User.class, id);
	}

	public void deleteUser(Long id) {

		User user = getUser(id);

		if (null != user) {
			getSession().delete(user);
		}

	}

	public User authUser(String username) {
		Query query = getSession().createQuery("from User where username='"+username+"'");
		User user = (User) query.uniqueResult();
		return user;
	}
	private Session getSession() {
		Session sess = getSessionFactory().getCurrentSession();
		if (sess == null) {
			sess = getSessionFactory().openSession();
		}
		return sess;
	}

	private SessionFactory getSessionFactory() {
		return sessionFactory;
	}

}
