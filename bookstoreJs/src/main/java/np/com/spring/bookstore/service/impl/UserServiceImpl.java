package np.com.spring.bookstore.service.impl;

import java.util.List;

import np.com.spring.bookstore.dao.UserDao;
import np.com.spring.bookstore.model.Book;
import np.com.spring.bookstore.model.User;
import np.com.spring.bookstore.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Transactional
	public void saveUser(User user) {
		userDao.saveUser(user);
	}

	@Transactional(readOnly = true)
	public List<User> listUsers() {
		return userDao.listUsers();
	}

	@Transactional(readOnly = true)
	public User getUser(Long id) {
		return userDao.getUser(id);
	}

	@Transactional
	public void deleteUser(Long id) {
		userDao.deleteUser(id);

	}
	
	@Transactional
	public User authUser(String username) {
		return userDao.authUser(username);
	}

}
