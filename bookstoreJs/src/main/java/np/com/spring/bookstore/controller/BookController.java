package np.com.spring.bookstore.controller;

import java.util.List;
import java.util.Map;

import np.com.spring.bookstore.model.Book;
import np.com.spring.bookstore.service.BookService;
import np.com.spring.bookstore.model.Order;
import np.com.spring.bookstore.service.OrderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

@Controller
@RequestMapping("/store")
public class BookController {

	@Autowired
	private BookService bookService;	
	@Autowired
	private OrderService orderService;

	
	@RequestMapping(method = RequestMethod.GET)
    public String getIndexPage() {
        return "index";
    }
	
    @RequestMapping(value = "/book", method = RequestMethod.GET)
    public ResponseEntity<List<Book>> listAllBooks() {
        List<Book> books = bookService.listBooks();
        if(books.isEmpty()){
            return new ResponseEntity<List<Book>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Book>>(books, HttpStatus.OK);
    }

	@RequestMapping("/get/{bookId}")
	public String getBook(@PathVariable Long bookId, Map<String, Object> map) {

		Book book = bookService.getBook(bookId);

		map.put("book", book);

		return "/book/bookForm";
	}

	@RequestMapping(value = "/book", method = RequestMethod.POST)
	public ResponseEntity<Void> createBook(@RequestBody Book book,    
			UriComponentsBuilder ucBuilder) {
		System.out.println("Creating Book " + book.getName());
		bookService.saveBook(book);

		HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/book/{id}").buildAndExpand(book.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/orderk", method = RequestMethod.POST)
	public ResponseEntity<Void> createOrder(@RequestBody Order order,    
			UriComponentsBuilder ucBuilder) {
		System.out.println("Creating Order " + order.getId());
		orderService.saveOrder(order);

		HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/book/{id}").buildAndExpand(order.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

    @RequestMapping(value = "/book/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Book> updateBook(@PathVariable("id") long id, @RequestBody Book book) {
        System.out.println("Updating Book " + id);
         
        Book currentBook = bookService.getBook(id);
         
        if (currentBook==null) {
            System.out.println("Book with id " + id + " not found");
            return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
        }
 
        currentBook.setName(book.getName());
        currentBook.setCode(book.getCode());
        currentBook.setIsbn(book.getIsbn());
        currentBook.setPrice(book.getPrice());
        currentBook.setPublishedOn(book.getPublishedOn());
        currentBook.setPublisher(book.getPublisher());
        currentBook.setAuthors(book.getAuthors());
         
        bookService.saveBook(currentBook);
        return new ResponseEntity<Book>(currentBook, HttpStatus.OK);
    }
 
    @RequestMapping(value = "/book/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Book> deleteBook(@PathVariable("id") long id) {
        System.out.println("Fetching & Deleting Book with id " + id);
 
        Book book = bookService.getBook(id);
        if (book == null) {
            System.out.println("Unable to delete. Book with id " + id + " not found");
            return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
        }
 
        bookService.deleteBook(id);
        return new ResponseEntity<Book>(HttpStatus.NO_CONTENT);
    }
 
}
