package np.com.spring.bookstore.dao.impl;

import java.util.List;

import np.com.spring.bookstore.dao.OrderDao;
import np.com.spring.bookstore.model.Order;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OrderDaoImpl implements OrderDao {

	@Autowired
	private SessionFactory sessionFactory;

	public void saveOrder(Order order) {
		getSession().merge(order);

	}

	@SuppressWarnings("unchecked")
	public List<Order> listOrders() {

		return getSession().createCriteria(Order.class).list();
	}

	public Order getOrder(Long id) {
		return (Order) getSession().get(Order.class, id);
	}

	public void deleteOrder(Long id) {

		Order order = getOrder(id);

		if (null != order) {
			getSession().delete(order);
		}

	}

	private Session getSession() {
		Session sess = getSessionFactory().getCurrentSession();
		if (sess == null) {
			sess = getSessionFactory().openSession();
		}
		return sess;
	}

	private SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}
