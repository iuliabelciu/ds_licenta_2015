package np.com.spring.bookstore.controller;

import java.util.List;
import java.util.Map;

import np.com.spring.bookstore.model.Order;
import np.com.spring.bookstore.service.OrderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

@Controller
@RequestMapping("/order")
public class OrderController {

	@Autowired
	private OrderService orderService;

	
	@RequestMapping(method = RequestMethod.GET)
    public String getIndexPage() {
        return "index";
    }
	
    @RequestMapping(value = "/order", method = RequestMethod.GET)
    public ResponseEntity<List<Order>> listAllOrders() {
        List<Order> orders = orderService.listOrders();
        if(orders.isEmpty()){
            return new ResponseEntity<List<Order>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Order>>(orders, HttpStatus.OK);
    }

	@RequestMapping("/get/{orderId}")
	public String getOrder(@PathVariable Long orderId, Map<String, Object> map) {

		Order order = orderService.getOrder(orderId);

		map.put("order", order);

		return "/order/orderForm";
	}

	@RequestMapping(value = "/order", method = RequestMethod.POST)
	public ResponseEntity<Void> createOrder(@RequestBody Order order,    
			UriComponentsBuilder ucBuilder) {
		System.out.println("Creating Order " + order.getId());
		orderService.saveOrder(order);

		HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/order/{id}").buildAndExpand(order.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

    @RequestMapping(value = "/order/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Order> updateOrder(@PathVariable("id") long id, @RequestBody Order order) {
        System.out.println("Updating Order " + id);
         
        Order currentOrder = orderService.getOrder(id);
         
        if (currentOrder==null) {
            System.out.println("Order with id " + id + " not found");
            return new ResponseEntity<Order>(HttpStatus.NOT_FOUND);
        }
 
        currentOrder.setId(order.getId());
        currentOrder.setUserId(order.getUserId());
        currentOrder.setTotalCost(order.getTotalCost());
        currentOrder.setDate(order.getDate());
         
        orderService.saveOrder(currentOrder);
        return new ResponseEntity<Order>(currentOrder, HttpStatus.OK);
    }
 
    @RequestMapping(value = "/order/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Order> deleteOrder(@PathVariable("id") long id) {
        System.out.println("Fetching & Deleting Order with id " + id);
 
        Order order = orderService.getOrder(id);
        if (order == null) {
            System.out.println("Unable to delete. Order with id " + id + " not found");
            return new ResponseEntity<Order>(HttpStatus.NOT_FOUND);
        }
 
        orderService.deleteOrder(id);
        return new ResponseEntity<Order>(HttpStatus.NO_CONTENT);
    }
 
}
