package np.com.spring.bookstore.service.impl;

import java.util.List;

import np.com.spring.bookstore.dao.OrderDao;
import np.com.spring.bookstore.model.Book;
import np.com.spring.bookstore.model.Order;
import np.com.spring.bookstore.service.OrderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderDao orderDao;

	@Transactional
	public void saveOrder(Order order) {
		orderDao.saveOrder(order);
	}

	@Transactional(readOnly = true)
	public List<Order> listOrders() {
		return orderDao.listOrders();
	}

	@Transactional(readOnly = true)
	public Order getOrder(Long id) {
		return orderDao.getOrder(id);
	}

	@Transactional
	public void deleteOrder(Long id) {
		orderDao.deleteOrder(id);

	}

}
