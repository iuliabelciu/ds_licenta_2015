package np.com.spring.bookstore.dao;

import java.util.List;
import np.com.spring.bookstore.model.User;

public interface UserDao {

	/*
	 * CREATE and UPDATE
	 */
	public void saveUser(User user); // create and update

	/*
	 * READ
	 */
	public List<User> listUsers();
	public User getUser(Long id);

	/*
	 * DELETE
	 */
	public void deleteUser(Long id);
    
	/*
	 * AUTHENTICATE
	 */
	public User authUser(String username);
}
