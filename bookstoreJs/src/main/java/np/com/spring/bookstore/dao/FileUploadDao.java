package np.com.spring.bookstore.dao;



import java.util.List;

import np.com.spring.bookstore.model.UploadedFile;

public interface FileUploadDao {

  List<UploadedFile> listFiles();

  UploadedFile getFile(Long id);

  UploadedFile saveFile(UploadedFile uploadedFile);

}
