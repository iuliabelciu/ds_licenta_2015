package np.com.spring.bookstore.dao;

import java.util.List;
import np.com.spring.bookstore.model.Order;

public interface OrderDao {

	/*
	 * CREATE and UPDATE
	 */
	public void saveOrder(Order order); // create and update

	/*
	 * READ
	 */
	public List<Order> listOrders();
	public Order getOrder(Long id);

	/*
	 * DELETE
	 */
	public void deleteOrder(Long id);
}
