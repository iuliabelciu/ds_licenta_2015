package np.com.spring.bookstore.service;

import java.util.List;

import np.com.spring.bookstore.model.Order;

public interface OrderService {

	/*
	 * CREATE and UPDATE 
	 */
	public void saveOrder(Order order);

	/*
	 * READ
	 */
	public List<Order> listOrders();
	public Order getOrder(Long id);

	/*
	 * DELETE
	 */
	public void deleteOrder(Long id);

}
