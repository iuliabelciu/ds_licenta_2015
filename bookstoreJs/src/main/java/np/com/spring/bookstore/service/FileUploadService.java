package np.com.spring.bookstore.service;



import java.util.List;

import np.com.spring.bookstore.model.UploadedFile;

public interface FileUploadService {

  List<UploadedFile> listFiles();

  UploadedFile getFile(Long id);

  UploadedFile saveFile(UploadedFile uploadedFile);

}
