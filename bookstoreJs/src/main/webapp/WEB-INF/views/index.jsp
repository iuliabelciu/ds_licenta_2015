<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
  <head>  
    <title>BookstoreJs</title>  

    <link rel="stylesheet" href='<c:url value="/web-resources/css/bootstrap.css"/>'>
     <link rel="stylesheet" href='<c:url value="/web-resources/css/app.css"/>'>
  </head>
  	<body ng-app="myApp">
     	  
		<div ng-view></div>
      
      <script src='<c:url value="/web-resources/js/angular.js"/>'></script>
      <script src='<c:url value="/web-resources/js/angular-resource.js"/>'></script>
      <script src='<c:url value="/web-resources/js/angular-route.js"/>'></script>
      <script src='<c:url value="/web-resources/js/angular-cookies.js"/>'></script>
      <script src='<c:url value="/web-resources/js/app.js"/>'></script>
      <script src='<c:url value="/web-resources/js/service/user_service.js"/>'></script>  
      <script src='<c:url value="/web-resources/js/controller/user_controller.js"/>'></script>
      <script src='<c:url value="/web-resources/js/service/store_service.js"/>'></script>  
      <script src='<c:url value="/web-resources/js/controller/store_controller.js"/>'></script>

  </body>
</html>
