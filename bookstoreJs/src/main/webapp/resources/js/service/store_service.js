'use strict';

App.factory('StoreService', ['$http', '$q', function($http, $q){

	return {
		
			fetchAllBooks: function() {
					return $http.get('http://localhost:8080/bookstoreJs/store/book')
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while fetching books');
										return $q.reject(errResponse);
									}
							);
			},

		    createBook: function(book){
					return $http.post('http://localhost:8080/bookstoreJs/store/book', book)
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while creating book');
										return $q.reject(errResponse);
									}
							);
		    },

		    createOrder: function(order){
					return $http.post('http://localhost:8080/bookstoreJs/store/order', order)
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while creating order');
										return $q.reject(errResponse);
									}
							);
		    },
		    
		    updateBook: function(book, id){
					return $http.put('http://localhost:8080/bookstoreJs/store/book/'+id, book)
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while updating book');
										return $q.reject(errResponse);
									}
							);
			},
		    
			deleteBook: function(id){
					return $http.delete('http://localhost:8080/bookstoreJs/store/book/'+id)
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while deleting book');
										return $q.reject(errResponse);
									}
							);
			}
		
	};

}]);
