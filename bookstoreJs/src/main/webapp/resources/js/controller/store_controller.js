'use strict';

App.controller('StoreController', ['$scope', 'StoreService','$routeParams','$location', function($scope, StoreService, $routeParams, $location) {
          var self = this;
          self.book={id:null,name:'',authors:'',price:'',code:'',isbn:'',publisher:'',publishedOn:''};
          self.books=[];
          self.cartName = 'Guest';
          self.clearCart = false;
          self.checkoutParameters = {};
          self.items = [];
          
   	       // load items from local storage
   	   	  self.loadItems = function () {
   	      var items = localStorage != null ? localStorage[this.cartName + "_items"] : null;
   	      if (items != null && JSON != null) {
   	           try {
   	               var items = JSON.parse(items);
   	               for (var i = 0; i < items.length; i++) {
   	                   var item = items[i];
   	                   if (item.id != null && item.name != null && item.authors != null && item.price != null && item.quantity != null) {
   	                       item = new cartItem(item.id, item.name, item.authors, item.price, item.quantity);
   	                       this.items.push(item);
   	                   }
   	               }
   	           }
   	           catch (err) {
   	               // ignore errors while loading...
   	           }
   	       }
   	   }

          // load items from local storage when initializing
          self.loadItems();
                    
          self.fetchAllBooks = function(){
              StoreService.fetchAllBooks()
                  .then(
      					       function(d) {
      						        self.books = d;
      					       },
            					function(errResponse){
            						console.error('Error while fetching Currencies');
            					}
      			       );
          };
          
          self.createBook = function(book){
              StoreService.createBook(book)
		              .then(
                      self.fetchAllBooks, 
				              function(errResponse){
					               console.error('Error while creating Book.');
				              }	
                  );
          };  
          
          self.createOrder = function(order){
              OrderService.createOrder(order);
          };

         self.updateBook = function(book, id){
              StoreService.updateBook(book, id)
		              .then(
				              self.fetchAllBooks, 
				              function(errResponse){
					               console.error('Error while updating Book.');
				              }	
                  );
          };

         self.deleteBook = function(id){
              StoreService.deleteBook(id)
		              .then(
				              self.fetchAllBooks, 
				              function(errResponse){
					               console.error('Error while deleting Book.');
				              }	
                  );
          };

          self.fetchAllBooks();

          self.submit = function() {
              if(self.book.id==null){
                  console.log('Saving New Book', self.book);    
                  self.createBook(self.book);
              }else{
                  self.updateBook(self.book, self.book.id);
                  console.log('Book updated with id ', self.book.id);
              }
              self.reset();
          };
              
          self.edit = function(id){
              console.log('id to be edited', id);
              for(var i = 0; i < self.books.length; i++){
                  if(self.books[i].id == id) {
                     self.book = angular.copy(self.books[i]);
                     break;
                  }
              }
          };
              
          self.remove = function(id){
              console.log('id to be deleted', id);
              for(var i = 0; i < self.books.length; i++){
                  if(self.books[i].id == id) {
                     self.reset();
                     break;
                  }
              }
              self.deleteBook(id);
          };

          
          self.reset = function(){
              self.book={id:null,name:'',address:'',email:''};
          };

          
	      self.toNumber = function (value) {
	    	    value = value * 1;
	    	    return isNaN(value) ? 0 : value;
	    	};
	      
	       // adds an item to the cart
	      self.addItem = function (id,name,authors,price,quantity) {
              console.log('id added to card', id);
              quantity = this.toNumber(quantity);
              if (quantity != 0) {

                  // update quantity for existing item
                  var found = false;
                  for (var i = 0; i < this.items.length && !found; i++) {
                      var item = this.items[i];
                      if (item.id == id) {
                          found = true;
                          item.quantity = this.toNumber(item.quantity + quantity);
                          if (item.quantity <= 0) {
                              this.items.splice(i, 1);
                          }
                      }
                  }

                  // new item, add now
                  if (!found) {
                      var item = new cartItem(id, name, authors, price, quantity);
                      this.items.push(item);
                  }

                  // save changes
                  this.saveItems();
              
	       };
	
	       // get the total price for all items currently in the cart
	       self.getTotalPrice = function (id) {
	           var total = 0;
	           for (var i = 0; i < this.items.length; i++) {
	               var item = this.items[i];
	               if (id == null || item.id == id) {
	                   total += this.toNumber(item.quantity * item.price);
	               }
	           }
	           return total;
	       };
	
	       // get the total price for all items currently in the cart
	       self.getTotalCount = function (id) {
	           var count = 0;
	           for (var i = 0; i < this.items.length; i++) {
	               var item = this.items[i];
	               if (id == null || item.id == id) {
	                   count += this.toNumber(item.quantity);
	               }
	           }
	           return count;
	       }
	      };
	      
	      function cartItem(id, name, authors, price, quantity) {
	    	    this.id = id;
	    	    this.name = name;
	    	    this.authors = authors;
	    	    this.price = price * 1;
	    	    this.quantity = quantity * 1;
	      }
       
	   // save items to local storage
	   self.saveItems = function () {
	       if (localStorage != null && JSON != null) {
	           localStorage[this.cartName + "_items"] = JSON.stringify(this.items);
	       }
	   }
	  
       
       self.clearItems = function () {
    	    this.items = [];
    	    this.saveItems();
    	}
       
       self.clearItem = function (id) {
   	    this.items = [];
   	    this.saveItems();
   		}
       
       self.checkout = function (parms, clearCart) {

    	    // global data
    	    var data = {};

    	    // item data
    	    for (var i = 0; i < this.items.length; i++) {
    	        var item = this.items[i];
    	        var ctr = i + 1;
    	        data["item_name_" + ctr] = item.name;
    	        data["item_author_" + ctr] = item.author;
    	        data["item_id_" + ctr] = item.id;
    	        data["item_price_" + ctr] = item.price.toFixed(2);
    	        data["item_quantity_" + ctr] = item.quantity;
    	    }

    	    var order = {id: Math.random().toString(16).slice(2), userId: Math.random().toString(16).slice(2), totalCost: self.totalCost(), date: Date.now()}
    	    
    	    self.createOrder(order);
    	    // submit form
    	    this.clearCart = clearCart == null || clearCart;
    	    form.submit();
    	    form.remove();
    	}
       
       self.productPage = function(id){
           for(var i = 0; i < self.books.length; i++){
               if(self.books[i].id == id) {
           	    self.book.id = self.books[i].id; 
        	    self.book.name = self.books[i].name;
        	    self.book.authors = self.books[i].authors;
        	    self.book.price = self.books[i].price;
        	    self.book.code = self.books[i].code;
        	    self.book.isbn = self.books[i].isbn;
        	    self.book.publisher = self.books[i].publisher;
        	    self.book.publishedOn = self.books[i].publishedOn;
               }
           }
           $location.path("/product");  
    	}
       
       function getURLParameter(name) {
    	   return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
    	 }
}]);
