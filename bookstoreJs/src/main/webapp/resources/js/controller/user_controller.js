'use strict';

App.controller('UserController', ['$scope', 'UserService', '$rootScope', '$location', function($scope, UserService, $rootScope, $location) {
          var self = this;
          self.user={id:null,username:'',password:'',type:'',address:'',email:''};
          self.users=[];
          self.currentUser={id:null,username:'',password:'',type:'',address:'',email:''};
              
          self.fetchAllUsers = function(){
              UserService.fetchAllUsers()
                  .then(
      					       function(d) {
      						        self.users = d;
      					       },
            					function(errResponse){
            						console.error('Error while fetching Currencies');
            					}
      			       );
          };
           
          self.createUser = function(user){
              UserService.createUser(user)
		              .then(
                      self.fetchAllUsers, 
				              function(errResponse){
					               console.error('Error while creating User.');
				              }	
                  );
          };

         self.updateUser = function(user, id){
              UserService.updateUser(user, id)
		              .then(
				              self.fetchAllUsers, 
				              function(errResponse){
					               console.error('Error while updating User.');
				              }	
                  );
          };

         self.deleteUser = function(id){
              UserService.deleteUser(id)
		              .then(
				              self.fetchAllUsers, 
				              function(errResponse){
					               console.error('Error while deleting User.');
				              }	
                  );
          };

          self.fetchAllUsers();

          self.submit = function() {
              if(self.user.id==null){
                  console.log('Saving New User', self.user);    
                  self.createUser(self.user);
              }else{
                  self.updateUser(self.user, self.user.id);
                  console.log('User updated with id ', self.user.id);
              }
              self.reset();
          };
          
          self.login = function() {
              self.authUser(self.user.username,self.user.password);
          };
          
          self.edit = function(id){
        	  UserService.getUser(id)
                      .then(
     					       function(u) {
     						        self.user = u;
     					       },
           					function(errResponse){
           						console.error('Error while fetching user');
           					}
     			       );
          }
          
          self.authUser = function(username, password){
        	  UserService.authUser(username)
                      .then(
     					       function(u) {
     					    	    if(password==u.password){
     					    	    	self.currentUser = u;
     					    	    	$rootScope.currentUser=u;
     					    	    	if(u.type=="user")
     					    	    		$location.path("/products");
     					    	    	if(u.type=="admin")
     					    	    		$location.path("/books");  
     					    	    }
     					       },
           					function(errResponse){
           						console.error('Error while fetching user');
           					}
     			       );
          }
          
          self.remove = function(id){
              console.log('id to be deleted', id);
              for(var i = 0; i < self.users.length; i++){
                  if(self.users[i].id == id) {
                     self.reset();
                     break;
                  }
              }
              self.deleteUser(id);
          };

          self.reset = function(){
              self.user={id:null,username:'',password:'',type:'',address:'',email:''};
              $scope.myForm.$setPristine(); 
          };
          
          self.signout = function(){
   	    	self.currentUser = null;
 	    	$rootScope.currentUser=null; 	    		
 	    	$location.path("/");  
          }

      }]);
