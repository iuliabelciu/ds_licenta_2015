'use strict';

var App = angular.module('myApp',['ngRoute','ngResource','ngCookies'])
       			 .config(config)
    config.$inject = ['$routeProvider', '$locationProvider'];
    function config($routeProvider, $locationProvider) {
    $routeProvider
    .when('/',{
    	templateUrl: 'web-resources/partials/login.html',      
    	controller: 'UserController',
        controllerAs: 'ctrl'
    })
    .when('/login',{
    	templateUrl: 'web-resources/partials/login.html',
    	controller: 'UserController',
        controllerAs: 'ctrl'
    })
    .when('/register',{
    	templateUrl: 'web-resources/partials/register.html',
    	controller: 'UserController',
        controllerAs: 'ctrl'
    })
    .when('/users',{
    	templateUrl: 'web-resources/partials/users.html',
    	controller: 'UserController',
        controllerAs: 'ctrl', 
        resolve: {
            factory: checkRouting
        }
    })
    .when('/books',{
    	templateUrl: 'web-resources/partials/books.html',
    	controller: 'StoreController',
        controllerAs: 'ctrl', 
        resolve: {
            factory: checkRouting
        }
    })
    .when('/products',{
    	templateUrl: 'web-resources/partials/store.html',
    	controller: 'StoreController',
        controllerAs: 'ctrl', 
        resolve: {
            factory: checkRouting
        }
    })
    .when('/cart', {
        templateUrl: 'web-resources/partials/cart.html',
        controller: 'StoreController',
        controllerAs: 'ctrl', 
        resolve: {
            factory: checkRouting
        }
    })  
    .when('/logout', {
        templateUrl: 'web-resources/partials/login.html',
        resolve: {
            factory: checkRouting
        }
    })      
    .when('/product/', {
        templateUrl: 'web-resources/partials/product.html',
        controller: 'StoreController',
        controllerAs: 'ctrl', 
        resolve: {
            factory: checkRouting
        }
    })     
    .when('/profile', {
        templateUrl: 'web-resources/partials/profile.html',
        controller: 'UserController',
        controllerAs: 'ctrl', 
        resolve: {
            factory: checkRouting
        }
    })
    .otherwise({redirectTo:'/'});
};    

var checkRouting= function ($q, $rootScope, $location) {  
	/*if($location.path()=="/logout"){
		$rootScope.currentUser=null;
	}
    if ($rootScope.currentUser) {
    	return true;  
    } else {
        $location.path("/");     
    }   
    */
	return true;
};